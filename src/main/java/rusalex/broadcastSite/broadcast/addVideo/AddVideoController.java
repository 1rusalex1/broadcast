package rusalex.broadcastSite.broadcast.addVideo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rusalex.broadcastSite.broadcast.video.VideoFile;
import rusalex.broadcastSite.broadcast.video.VideoFileService;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.*;

/**
 * Created by admin on 23.02.2016.
 */

@Controller
public class AddVideoController {

    private static final String VIDEO_VIEW_NAME = "addvideo/addvideo";
    @Autowired
    ServletContext servletContext;
    @Autowired
    ResourceLoader resourceLoader;
    @Autowired
    VideoFileService videoFileService;

    public AddVideoController() {
    }

    @RequestMapping(value = "addvideo", method = RequestMethod.GET)
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public String addVideo(Model model) {
        model.addAttribute(new AddVideoForm());
        return VIDEO_VIEW_NAME;
    }

    @RequestMapping(value = "addvideo", method = RequestMethod.POST, headers = "Content-Type=multipart/form-data")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ResponseBody
    public Map<String, List<String>> addVideo(@Valid AddVideoForm addVideoForm, Errors errors) throws IOException {


        Map<String, List<String>> response = new HashMap<String, List<String>>();
        if (errors.hasErrors()) {
            List<FieldError> fieldErrors = errors.getFieldErrors();
            List<String> errorsMap = new ArrayList<String>();
            for (FieldError field : fieldErrors) {
                errorsMap.add(field.getField());
            }
            response.put("errors", errorsMap);
            return response;
        }
        MultipartFile file = addVideoForm.getVideoFile();
        if (!file.isEmpty()) {
            Long timestamp = new Date().getTime();
            String fileName = timestamp + "_" + file.getOriginalFilename();
            //не работает, так как выполняется в параллельном потоке и файл не успевает скопироваться из временной директории
            // file.transferTo(resourceLoader.getResource("resources/videos/" + fileName).getFile());
            byte[] bytes = file.getBytes();
            String filePath = servletContext.getRealPath("/resources/videos/" + fileName);
            File serverFile = new File(filePath);
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();

            videoFileService.save(new VideoFile(addVideoForm.getTitle(), addVideoForm.getDescription(), fileName));

            response.put("success", null);

        }
        return response;
    }

    @RequestMapping(value = "updatevideo", method = RequestMethod.GET)
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public String updateVideo(Model model, Principal principal) {
        model.addAttribute(new AddVideoForm());
        return VIDEO_VIEW_NAME;
    }

    @RequestMapping(value = "getMyVideos/{pageNumber}", method = RequestMethod.GET)
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public String getMyVideos(@PathVariable Integer pageNumber, Model model, Principal principal) {
        String username;
        username = principal.getName();
        List<VideoFile> videoFiles = videoFileService.getVideoFilesByAccount();
        model.addAttribute("videoFiles", videoFiles);
        return "getVideos/getMyVideos";
    }
}
