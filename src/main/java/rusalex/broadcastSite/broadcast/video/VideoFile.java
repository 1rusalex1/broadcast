
package rusalex.broadcastSite.broadcast.video;

import org.hibernate.validator.constraints.NotEmpty;
import rusalex.broadcastSite.account.Account;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;


/**
 * Created by admin on 18.02.2016.
 */


@Entity
@Table(name = "VideoFile")
public class VideoFile implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    /*   @NotEmpty*/
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id")
    private Account account;

    private String fileName;

    private String title;

    private String description;

    private Instant uploaded;

    protected VideoFile() {

    }

    public VideoFile(String title, String description, String fileName) {
        this.title = title;
        this.fileName = fileName;
        this.uploaded = Instant.now();
        this.description = description;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Instant getUploaded() {
        return uploaded;
    }

    public Long getId() {
        return id;
    }
}

