package rusalex.broadcastSite.broadcast.video;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rusalex.broadcastSite.account.Account;

import java.util.List;

/**
 * Created by admin on 18.02.2016.
 */
@Repository
public interface VideoFileRepository extends JpaRepository<VideoFile, Long> {
    public List<VideoFile> findVideoFilesByAccount(Account account);
}
