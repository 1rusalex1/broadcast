package rusalex.broadcastSite.broadcast.video;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rusalex.broadcastSite.account.Account;
import rusalex.broadcastSite.account.AccountRepository;

import java.security.Principal;
import java.util.List;

/**
 * Created by admin on 18.02.2016.
 */
@Service
public class VideoFileService {

    @Autowired
    private VideoFileRepository videoFileRepository;

    @Autowired
    private AccountRepository accountRepository;


    private Account getAccount() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return accountRepository.findOneByEmail(username);
    }

    @Transactional
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public VideoFile save(VideoFile videoFile) {
        Account account = getAccount();
        videoFile.setAccount(account);
        videoFileRepository.save(videoFile);
        return videoFile;
    }

    @Transactional
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public List<VideoFile> getVideoFilesByAccount() {
        Account account = getAccount();
        return videoFileRepository.findVideoFilesByAccount(account);
    }


}
