package rusalex.broadcastSite.home;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import rusalex.broadcastSite.broadcast.video.VideoFile;
import rusalex.broadcastSite.broadcast.video.VideoFileRepository;

@Controller
public class HomeController {

    @Autowired
    VideoFileRepository videoFileRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Principal principal, Model model) {
        List<VideoFile> videoFiles = videoFileRepository.findAll();
        model.addAttribute("videoFiles", videoFiles);
        return principal != null ? "home/homeSignedIn" : "home/homeNotSignedIn";
    }
}
