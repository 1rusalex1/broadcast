/**
 * Created by admin on 24.02.2016.
 */


(function ($) {
    var fileInputButton = $('#fileInputButton');
    var errors = [];
    var fileInput = $('#fileupload');

    /* fileInputButton.click(function () {
     fileInput.click();
     });*/

    $("#progress").find("img").click(function () {
        removeFile($('#fileupload'))
    });

    fileInput.fileupload({
        dataType: 'json',
        autoUpload: false,
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress').find('.bar').css(
                'width',
                progress + '%'
            );
        },
        add: function (e, data) {
            removeFile(this);
            var uploadErrors = [];
            var maxFileSize = 10000000;
            var acceptFileTypes = /(\.|\/)(mp4|webm|ogg)$/i;  //mp4|avi|mov|wmv|3gpp|x-flv|webm|ogg
            var fileErrorBlock = $("#fileError");
            if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                uploadErrors.push('Неподходящий тип файла');
            }
            if (data.originalFiles[0]['size'] > maxFileSize) {
                uploadErrors.push('Размер файла превышает допустимый.');
            }
            if (uploadErrors.length > 0) {
                fileErrorBlock.html(uploadErrors[0]);
                addError('file');
                showErrors();
            } else {
                var fileName = data.originalFiles[0]['name'];
                $("#fileName").attr("title", fileName).html(cutFileName(fileName));
                $(this).data().preupload = data;
                $("#progress").removeClass("hidden");
            }
        },
        done: function (e, data) {
            if (data.result.success !== undefined) {
                document.location.replace("/");
            }
        }
    });

    function removeFile(obj) {
        var file = $(obj).data().preupload;
        if (file != undefined) {
            delete $(obj).data().preupload;
        }
        $("#fileupload").val("");
        $("#progress").addClass("hidden");
    }

    function cutFileName(filename) {
        var extension = filename.substr(filename.lastIndexOf('.'));
        var extension_length = extension.length;
        var basis_name = filename.substr(0, filename.lastIndexOf('.'));
        return basis_name.substr(0, (14 - extension_length)) + "... ." + extension;
    }

    function addError(errorType) {
        errors.push(errorType);
    }

    function cleanErrors() {
        errors = [];
    }

    function showErrors() {
        errors.forEach(function (item) {
            $("#" + item + "Error").removeClass("hidden").parent().addClass("has-error");
        })
    }

    function hideErrors() {
        $(".has-error").removeClass('has-error').children(".help-block").addClass("hidden");
    }

    function validateForm() {
        var fileInput = $('#fileupload');
        if (fileInput.data().preupload == undefined || fileInput.data().preupload.files.length == 0) {
            addError("file");
        }
        $(".require").each(function () {
            var $this = $(this);
            if ($this.val().trim() == "") {
                var name = $this.attr("name");
                addError(name);
            }
        });
        return errors.length <= 0;

    }

    function submitForm() {
        var fileInput = $('#fileupload'),
            formData = new FormData;
        formData.append('title', $("#title").val());
        formData.append('description', $("#description").val());
        fileInput.fileupload({
            formData: formData
        });
        fileInput.data().preupload.submit();
    }

    $("#submitVideoForm").on('click', function () {
        hideErrors();
        cleanErrors();
        if (validateForm()) {
            submitForm();
        } else {
            showErrors();
        }
    });


})(jQuery);